// ---------------------------------------------------------------------------------------------------------
// filename:	DAMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		24.10.2003
// version:		20031024
// description:	DAta Message. Used to send data between nodes.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	DAM		DAta Message
//				data		-	1011	-	3..1014		-	The data to be sendt
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class DAMessage {

	private String data;
	Debug d;


	public DAMessage(String data_){
		d		= new Debug("DAMessage");
		data	= data_;
	}


	public byte[] flushAsBytes(){
		byte[] bytes = new byte[1014];
		byte[] dataBytes = new byte[1014];
		int y;
		int index;
		int charvalue;

		// -----------
		// identifier
		// -----------
		bytes[0] = 68; // D
		bytes[1] = 65; // A
		bytes[2] = 77; // M
		
		// -----------
		// data
		// -----------		
		if ( data.length()>0 ) {
			dataBytes = data.getBytes();

			for (y=0; y<data.length(); y++){
				bytes[3+y] = dataBytes[y];
			}
		}
		return bytes;
	}
}
