// ---------------------------------------------------------------------------------------------------------
// filename:	RIPMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		15.10.2003
// version:		20031018
// description:	Routing Information Protocol. Used by routers to excchange routing table information.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	RIP		Routing Information Protocol
//				noEntries	-	1		-	3			-	0..255	Number of entries (distination/distance tuples)
//				free		-	2		-	4..5		-	Not in use
//				destination -	5		-	(6..10)*X	-	The destination address	| repeated "noEntries" times
//				distance	-	5		-	(11..15)*X	-	The distance			| ----------- " ------------
//
//
//				-------------------------------------------------------
//				Format of RIP message:
//                                                [- - repeated - -]
//				IDENTIFIER - NO. ENTRIES - FREE - ADDRESS - DISTANCE
//				[3 bytes]  - [1 byte]    - [2b] - [7*144=1008 bytes]
//				[- - - - - - - - -  1014 bytes - - - - - - - - - - ]
//				-------------------------------------------------------
//
// ---------------------------------------------------------------------------------------------------------

package message;

import java.util.Vector;

import utility.Debug;
import routingtable.RoutingTable;


public class RIPMessage {

	private Vector destinations;
	private Vector distances;
	private Debug d;


	public RIPMessage(String routerID){
		d 				= new Debug("RIPMessage '"+routerID+"'");
		destinations	= new Vector();
		distances		= new Vector();
	}

	
	public RIPMessage(String routerID, RoutingTable routingTable){
		d 				= new Debug("RIPMessage '"+routerID+"'");
		destinations	= routingTable.getDestinations();
		distances		= routingTable.getDistances();	
	}

	
	public void addEntry(String destination_, String distance_){
		// in case number is less than 10, we add a zero in front ('1'->'01')
		if (distance_.length()<2)
			distance_ = "0"+distance_;
		
		//d.print("dest:"+destination_+" dist:"+distance_);
		destinations.add(destination_);
		distances.add(distance_);
	}
	
	
	public Vector getDestinations(){
		return destinations;
	}


	public Vector getDistances(){
		return distances;
	}


	public byte[] flushAsBytes(){
		byte[] bytes = new byte[1014];
		String noEntries;
		String destination;
		String distance;
		int i;
		int y;
		int charvalue;
		int index=0;
		

		// -----------
		// Identifier
		// -----------
		bytes[0] = 82; // R
		bytes[1] = 73; // I
		bytes[2] = 80; // P
		
		// -----------
		// Number of entries
		// -----------
		noEntries = Integer.toString(destinations.size());		
		bytes[3] = (new Integer(noEntries)).byteValue();

		// -----------
		// Free, not in use
		// -----------
		bytes[4] = 0;
		bytes[5] = 0;

		// -----------
		// destinations and distances
		// -----------
		for (i=0; i<destinations.size(); i++){
			destination = (String)destinations.get(i);
			distance = (String)distances.get(i);
			//d.print("dest: "+destination+" dist:"+distance);

			// -----------
			// destination
			// -----------
			for (y=0; y<5; y++){
				charvalue = new Integer(destination.substring(y,y+1)).intValue() + 48; // ascii 48 = '0'
				index = 6 + (7*i) + y;
				bytes[index] = (byte)charvalue;
				//d.print("destination.bytes["+index+"]: "+new String(bytes,index,1));
			}

			// -----------
			// distance
			// -----------
			for (y=0; y<2; y++){
				charvalue = new Integer(distance.substring(y,y+1)).intValue() + 48; // ascii 48 = '0'
				index = 11 + (7*i) + y;
				bytes[index] = (byte)charvalue;
				//d.print("distance.bytes["+index+"]: "+new String(bytes,index,1));
			}
		}		
		return bytes;
	}
}
