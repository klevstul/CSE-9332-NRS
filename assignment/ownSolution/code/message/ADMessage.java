// ---------------------------------------------------------------------------------------------------------
// filename:	ADMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description:	Agent Discovery Message, router sends this to connected nodes. This contains information 
//				about the router. It tells if the router is a home agent (HA) a foreign agent (FA) or 
//				just a basic router.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	ADM		Agent Discovery Message
//				code		-	3		-	3..5		-	HFA		home-/foreign agent
//															000		basic router
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class ADMessage {

	private String code;
	Debug d;

	public ADMessage(String code_){
		code 		= code_;
		d			= new Debug("ADMessage");
	}

	public byte[] flushAsBytes(){
		byte[] bytes = new byte[6];

		// -----------
		// identifier
		// -----------
		bytes[0] = 65; // A
		bytes[1] = 68; // D
		bytes[2] = 77; // M
		

		// -----------
		// code
		// -----------
		if (code.equals("HFA")){
			bytes[3] = 72; // H
			bytes[4] = 70; // F
			bytes[5] = 65; // A
		} else {
			bytes[3] = 0;
			bytes[4] = 0;
			bytes[5] = 0;
		}
		
		return bytes;
	}
}
