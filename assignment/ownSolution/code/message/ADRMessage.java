// ---------------------------------------------------------------------------------------------------------
// filename:	ADRMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description:	Agent Discorvery Request message. Is used when a node wants to receive an ADM message from
//				the router. When a router receives an ADR message, it replies by sending an ADM message.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	ADR		Agent Discovery Request message
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class ADRMessage {

	Debug d;

	public ADRMessage(){
		d = new Debug("ADRMessage");
	}

	public byte[] flushAsBytes(){
		byte[] bytes = new byte[3];

		// -----------
		// identifier
		// -----------
		bytes[0] = 65; // A
		bytes[1] = 68; // D
		bytes[2] = 82; // R
		
		return bytes;
	}
}
