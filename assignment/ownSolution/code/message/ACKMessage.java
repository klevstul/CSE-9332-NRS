// ---------------------------------------------------------------------------------------------------------
// filename:	ACKMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		24.10.2003
// version:		20031024
// description:	ACKMessage is used to send an ACKnowledgement
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	ACK		ACKnowledge
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class ACKMessage {

	Debug d;

	public ACKMessage(){
		d = new Debug("ACKMessage");
	}

	public byte[] flushAsBytes(){
		byte[] bytes = new byte[3];

		// -----------
		// identifier
		// -----------
		bytes[0] = 65; // A
		bytes[1] = 67; // C
		bytes[2] = 75; // K
		
		return bytes;
	}
}
