// ---------------------------------------------------------------------------------------------------------
// filename:	IPMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		17.10.2003
// version:		20031017
// description:	The IP message is the message used for sending data through the network. All other messages
//				are encapsulated in an IP message before it sendt.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				source		- 	5		-	0..4		-	The source, the address of the sender
//				destination	-	5		-	5..9		-	The destination
//				data		-	1014	-	10..1014	-	The encapsulated message, f.ex a DAM message
//
// ---------------------------------------------------------------------------------------------------------

package message;

import java.util.Vector;

import utility.Debug;


public class IPMessage {

	private String source;
	private String destination;
	private byte[] data;
	Debug d;


	public IPMessage(String source_, String destination_, byte[] data_){
		source 		= source_;
		destination = destination_;
		data 		= new byte[1014];
		data 		= data_;
		d			= new Debug("IPMessage");
	}


	public String getSource(){
		return source;
	}


	public String getDestination(){
		return destination;
	}


	public byte[] flushAsBytes(){
		byte[] bsource 		= source.getBytes();
		byte[] bdestination	= destination.getBytes();
		byte[] returnbytes	= new byte[1024];
		int i;

		// ----------
		// source
		// ----------
		returnbytes[0] = bsource[0];
		returnbytes[1] = bsource[1];
		returnbytes[2] = bsource[2];
		returnbytes[3] = bsource[3];
		returnbytes[4] = bsource[4];

		// ----------
		// destination
		// ----------
		returnbytes[5] = bdestination[0];
		returnbytes[6] = bdestination[1];
		returnbytes[7] = bdestination[2];
		returnbytes[8] = bdestination[3];
		returnbytes[9] = bdestination[4];
		
		// ----------
		// data
		// ----------
		for (i=0; i<data.length; i++){
			returnbytes[10+i] = data[i];
		}
		return returnbytes;
	}
}
