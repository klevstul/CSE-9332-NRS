// ---------------------------------------------------------------------------------------------------------
// filename:	MREMessage.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description:	Mobile ip REply message. Used by home agents for signalising if a registration has been
//				successfull or not.
//
//				******************** content of message ******************
//				field		- bytesize	-	position	-	content desc
//				***********************************************************
//				identifier	- 	3		-	0..2		-	MRE		Mobile ip REply message
//				status		-	1		-	3			-	1		registration acknowledged
//															2		de-registration acknowledged
//															0		(de-)registration failed
//
// ---------------------------------------------------------------------------------------------------------

package message;

import utility.Debug;


public class MREMessage {

	private String status;
	Debug d;


	public MREMessage(String status_){
		d		= new Debug("MREMessage");
		status	= status_;
	}


	public byte[] flushAsBytes(){
		byte[] bytes = new byte[18];
		int y;
		int index;
		int charvalue;

		// -----------
		// identifier
		// -----------
		bytes[0] = 77; // M
		bytes[1] = 82; // R
		bytes[2] = 69; // E
		

		// -----------
		// status
		// -----------
		if (status.equals("ACK_reg"))
			bytes[3] = 1;
		else if (status.equals("ACK_dereg"))
			bytes[3] = 2;
		else
			bytes[3] = 0;

		return bytes;
	}
}
