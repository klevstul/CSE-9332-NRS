// ------------------------------------------------------
// filename:	GlobalConst.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		17.10.2003
// version:		20031015
// ------------------------------------------------------


// ---------------------------------------------------------
// NOTE: NOT IN USE!
// Should have been implemented to keep all global variables
// in one common file. 
// ---------------------------------------------------------

package global;


public interface GlobalConst {
}
