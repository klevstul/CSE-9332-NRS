// ---------------------------------------------------------------------------------------------------------
// filename:	Registration.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		23.10.2003
// version:		20031023
// description: Keeps a mobile node registered to a foreign agent. This is done by continuity sending 
//				request messages to the FA. It waits until the timer expires before it sends a new
//				registration request.
// ---------------------------------------------------------------------------------------------------------

package mobileipregistration;

import utility.Debug;
import utility.BusyWait;
import node.MobileNode;


public class Registration extends Thread {

	private BusyWait bw;
	private Debug d;
	private MobileNode mobileNode;
	

	public Registration(MobileNode mobileNode_){
		mobileNode	= mobileNode_;
		bw			= new BusyWait();
		d			= new Debug("Registration '"+mobileNode.getNodeID()+"'");
	}


	public void run() {
		while(true){
			if(mobileNode.isConnectedToFA())
				mobileNode.sendRegistrationMessage();

			bw.doWait(Integer.parseInt(mobileNode.getTimer()));
		}
	}
}
