// ---------------------------------------------------------------------------------
// filename:	PeriodicUpdate.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		18.10.2003
// version:		20031018
// description: Sends a periodic update RIP message to all the neighbours
// ---------------------------------------------------------------------------------

package task;

import java.util.TimerTask;

import node.Router;
import utility.Debug;
import message.RIPMessage;
import message.IPMessage;
import routingtable.RoutingTable;


public class PeriodicUpdate extends TimerTask {

	private Router router;


	public PeriodicUpdate(Router router_) {
		router = router_;
	}


	public void run(){
		Debug d					= new Debug("PeriodicUpdate '"+router.getAddress()+"'");
		byte[] ripdata 			= new byte[1014];
		byte[] ipdata 			= new byte[1024];
		int i					= 0;
		RIPMessage ripMessage 	= new RIPMessage(router.getRouterID(), router.getRoutingTable());
		ripdata 				= ripMessage.flushAsBytes();


		// -----------------------------------------------------
		// cancel the timer and schedule a new periodic update
		// -----------------------------------------------------
		router.cancelTimer();
		router.periodicUpdate();
		

		// -----------------------------------------------------
		// send message to all neighbours
		// -----------------------------------------------------
		for (i=0; i<router.getNeighbours().size(); i++){
			IPMessage ipMessage = new IPMessage(router.getAddress(), (String)router.getNeighbours().get(i), ripdata);
			//d.print("sending package to neighbour: "+ipMessage.getDestination());
			router.sendMessage(ipMessage, router);
		}
	}
}

