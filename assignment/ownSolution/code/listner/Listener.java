// -----------------------------------------------------------------------------------------------
// filename:	Listener.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		17.10.2003
// version:		20031018
// description:	A listner on a given port. Receives all data on this port, and sends it to a node 
//				to be processed. Implemented as a thread, so it can run all the time without 
//				stopping other actions.
// -----------------------------------------------------------------------------------------------

package listner;

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketAddress;

import utility.Debug;
import node.Node;
import node.Router;
import message.IPMessage;


public class Listener extends Thread {

	private String portnumber;
	private String source;
	private String destination;
	private String data;
	private byte[] receiveBytes;
	private boolean doLoop;
	private DatagramPacket receivePacket;
	private SocketAddress socketAddress;
	private Node node;
	private Debug d;
	private Router router;


	public Listener(String portnumber_, Node node_, Router router_){
		router			= router_;
		node			= node_;
		d				= new Debug("Listener '"+node.getAddress()+"'");
		portnumber		= portnumber_;
		receiveBytes	= new byte[1024];
		receivePacket	= new DatagramPacket(receiveBytes,receiveBytes.length);
		doLoop 			= true;
	}
	

	// ----------------------------------
	// starts the running of the thread
	// ----------------------------------
	public void run() {	
		//d.print("Listener is starting on port "+portnumber);
		
		try {
			DatagramSocket socket = new DatagramSocket(Integer.parseInt(portnumber));

			while (doLoop) {			
				socket.receive(receivePacket);
				socketAddress = receivePacket.getSocketAddress();

				source 		= new String(receiveBytes, 0, 5);
				destination	= new String(receiveBytes, 5, 5);
		        data		= new String(receiveBytes, 10, 1014);

				//d.print("source:"+source+" destination:"+destination+" data:"+data);

				// the message has reached the destination node
				if ( destination.equals(node.getAddress()) ){
					//d.print("node identified as destination node");
					node.processMessage(source, destination, data.getBytes());
				// forward the message
				} else {
					IPMessage ipMessage = new IPMessage(source, destination, data.getBytes());
					//d.print("forwarding message w/ source:'"+source+"' destination:'"+destination+"'");
					node.sendMessage(ipMessage, router);
				}				
			}
			socket.close();

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}


	// ---------------------------------
	// stop the running of the thread
	// ---------------------------------
	public void stopIt() {
		doLoop = false;
	}
}
