// ------------------------------------------------------
// filename:	Node.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		18.10.2003
// version:		20031018
// description: The parent for all nodes (routers and hosts)
// ------------------------------------------------------

package node;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

import listner.Listener;
import utility.Debug;
import message.IPMessage;
import node.Router;


abstract public class Node {

	private Debug d = new Debug("Node");
	protected String address;
	private Listener listener;


	public void sendMessage(IPMessage ipMessage, Router router){
		byte[] bytes = ipMessage.flushAsBytes();
		String destination = ipMessage.getDestination();
		String source = ipMessage.getSource();
		InetAddress ipAddress	= null;
		boolean msgNotSendt = true;
		String nextHop = "-";
		String verifiedDestination = null;
		

		// ----------------------------------------------
		// verfies the destination address
		// ----------------------------------------------
		if (router==null){
			// do nothing
		} else {
			verifiedDestination = router.getNewDestination(destination);
	
			if (verifiedDestination == null) {
				//d.print("destination not changed");
			} else {
				d.print("destination changed to: "+verifiedDestination);
				destination = verifiedDestination;
				
				// change destination in data package
				byte[] bdestination	= destination.getBytes();
				bytes[5] = bdestination[0];
				bytes[6] = bdestination[1];
				bytes[7] = bdestination[2];
				bytes[8] = bdestination[3];
				bytes[9] = bdestination[4];
			}
		}


		// -------------------------------------------------
		// finding the next node to send the package to
		// -------------------------------------------------
		if (router==null) {
			//d.print("A host is sending a message, has to go through it's router");
			nextHop = extractRouterIP(source);
		} else {
			// destination node is connected directly to this router, so send directly
			if (router.connectedToThisRouter(destination)) {
				//d.print("destination node connected directly to this router");
				nextHop = destination;
			} else {
				nextHop = router.getRoutingTable().getNextHop(extractRouterIP(destination));
				if (nextHop==null){
					//d.print("nexthop is null, we wont send anything....");
					return;
				} else if (nextHop.equals("-")){
					//d.print("directly connected to nexthop");
					nextHop = destination;
				}
			}
		}
		


		d.print("sending an IP message from '"+address+"' to '"+destination+"'"+" through '"+nextHop+"'");

		try {
			// sending back to same machine, since we just are simulating a network
			ipAddress = InetAddress.getByName("localhost");
		} catch (Exception e) {
			e.printStackTrace();
		}


		// ----------------------------
		// sending the message
		// ----------------------------
		while(msgNotSendt){
			try {
				DatagramPacket sendPacket	= new DatagramPacket(bytes, bytes.length, ipAddress, Integer.parseInt(nextHop));
	
				Random random = new Random();
				// a random portnumber: node port number minus a rand number from 0 - 1000
				long randPort = (new Long(address)).longValue() - (Math.abs(random.nextLong()) % 1000);
				//d.print("rand port: "+randPort);
				
				DatagramSocket socket		= new DatagramSocket((new Long(randPort)).intValue());
				socket.send(sendPacket);
				socket.close();
				msgNotSendt = false;
			} catch (Exception e) {
				d.print("Error sending message, try resending...");
				//e.printStackTrace();
			}
		}
	}


	public String getAddress(){
		return address;
	}


	public String extractRouterIP(String address){
		String routerIP = address;
		routerIP = routerIP.substring(0,4);
		routerIP = routerIP + "0";
		return routerIP;
	}


	public abstract void processMessage(String source, String destination, byte[] data);


	protected abstract void initialise(String nodeID);


	protected void setAddress(String address_){
		address = address_;
	}


	protected void startListner(String port, Node node, Router router){
		//d.print("starting a listener on port "+port);
		listener = new Listener(port,node,router);
		listener.start();
	}


	protected void stopListener(){
		if (listener==null) {
			// do nothing
		} else {
			listener.stopIt();
		}
	}
}
