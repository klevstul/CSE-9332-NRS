// ------------------------------------------------------
// filename:	MobileNode.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description: Implements a mobile node
// ------------------------------------------------------

package node;

import utility.Debug;
import utility.Message;
import mobileipregistration.Registration;
import message.MRQMessage;
import message.IPMessage;
import message.ACKMessage;


public class MobileNode extends SMNode {
	private Debug d;
	private String homeAddress;
	private String careOfAddress;
	private String timer;
	private Message msg;
	private boolean registered;

	public MobileNode(String nodeID){
		d				= new Debug("MobileNode '"+nodeID+"'");
		msg				= new Message("MobileNode '"+nodeID+"'");
		homeAddress		= "";
		careOfAddress	= "";
		registered		= false;
		timer 			= "15000";

		d.print("Object created");

		initialise(nodeID);
		
		keepNodeRegistered();
	}

	
	public void processMessage(String source, String destination, byte[] data){
		String identifier	= new String(data, 0, 3);
		//d.print("process message from '"+source+"' w/ identifier='"+identifier+"'");

		// ---------------------------
		// Agent Discovery Message
		// ---------------------------
		if (identifier.equals("ADM")){
			String code			= new String(data, 3, 3);
			
			// Home-/Foreign-Agent
			if (code.equals("HFA")){
				d.print("Received an Agent Discovery Message: '"+source+"' is an home-/foreign agent");
				if (homeAddress.length()==0){
					homeAddress = getAddress();
					d.print("Home adress is set to '"+homeAddress+"'");
				} else {
					careOfAddress = getAddress();
					d.print("Care-of-address is set to '"+careOfAddress+"'");
				}
			}
		// ---------------------------
		// Mobile ip REply message
		// ---------------------------
		} else if (identifier.equals("MRE")) {
			byte status	= data[3];
			if (status == 1) {
				d.print("Response message received from Home Agent, registration OK");
				registered = true;
			} else if (status == 2) {
				d.print("Response message received from Home Agent, de-registration OK");
				registered = true;
			} else {
				d.print("Response message received from Home Agent, (de-)registration NOT OK");
				registered = false;
			}
		// ---------------------------
		// DAta Message
		// ---------------------------
		} else if (identifier.equals("DAM")){
			String packagebody = new String(data, 3, 1010);
			String address;
			d.print("Data message received: '"+packagebody.substring(0,12)+"' sending ACKknowledge message back");

			if (careOfAddress.length()>0)
				address = careOfAddress;
			else
				address = getAddress();

			ACKMessage ackMessage = new ACKMessage();
			IPMessage ipMessage = new IPMessage(address, source, ackMessage.flushAsBytes());
			sendMessage(ipMessage, null);
		// ---------------------------
		// ACKnowledge message
		// ---------------------------
		} else if (identifier.equals("ACK")){
			d.print("ACKnowledge received");
		} else {
			msg.error("unknown message: '"+identifier+"'");
		}
	}
	
	
	// ----------------------------------
	// keep the node registered to a FA
	// ----------------------------------
	private void keepNodeRegistered(){
		Registration registration = new Registration(this);
		registration.start();
	}


	public String getTimer(){
		return timer;
	}


	public boolean isConnectedToFA(){
		if (homeAddress.length()>0 && careOfAddress.length()>0 && timer.length()>0)
			return true;
		
		return false;
	}


	// ------------------------------------
	// send a registration message to FA
	// ------------------------------------
	public void sendRegistrationMessage(){
		if ( homeAddress.length()>0 && careOfAddress.length()>0 ){
			MRQMessage mrqMessage = new MRQMessage(homeAddress, careOfAddress, timer);
			IPMessage ipMessage = new IPMessage(getAddress(), extractRouterIP(getAddress()), mrqMessage.flushAsBytes());
			d.print("Sending a Mobile IP Request Message to Foreign Agent (registration request)");	
			sendMessage(ipMessage, null);
		}
	}


	// ------------------------------------
	// send a de-registration message to FA
	// ------------------------------------
	public void sendDeRegistrationMessage(){
		if ( homeAddress.length()>0 && careOfAddress.length()>0 && registered ){
			MRQMessage mrqMessage = new MRQMessage(homeAddress, careOfAddress, "00000");
			IPMessage ipMessage = new IPMessage(getAddress(), extractRouterIP(getAddress()), mrqMessage.flushAsBytes());
			d.print("Sending a MRQ de-registration message to FA");
			sendMessage(ipMessage, null);
			registered = false;
		}
	}
}
