// ----------------------------------------------------------------------------------------
// filename:	ForeignAgent.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031024
// description: Implements a foreign agent (FA) router.
// ----------------------------------------------------------------------------------------

package node;

import java.util.Vector;

import utility.Debug;
import message.ADMessage;
import message.IPMessage;


public class ForeignAgent extends Router {

	private Debug d;
	Vector careOfAddresses;
	Vector homeAddresses;
	Vector expiryTimers;


	public ForeignAgent(String routerID){
		d				= new Debug("ForeignAgent '"+routerID+"'");
		careOfAddresses	= new Vector();
		homeAddresses	= new Vector();
		expiryTimers	= new Vector();
		
		d.print("Object created");
		
		// initialise routing table and set neighbours 
		initialise(routerID);
		
		// go to running mode
		run();
	}


	protected void sendADMessage(String destination){
		ADMessage adMessage = new ADMessage("HFA");
		IPMessage ipMessage = new IPMessage(getAddress(),destination,adMessage.flushAsBytes());
		sendMessage(ipMessage, this);
	}

	protected void processMRQMessage(String source, String destination, byte[] data){
		String homeAddress = new String(data, 3,5);
		String careOfAddress = new String(data, 8,5);
		String expiryTimer = new String(data, 13,5);
		IPMessage ipMessage = new IPMessage(getAddress(),extractRouterIP(homeAddress),data);
		int i;

		d.print("MRQmessage received");
		
		// de-registration ?
		if (expiryTimer.equals("00000")){
			//d.print("De-registration, removing all '"+homeAddress+"' entries in Foreign Agent");
			i = homeAddresses.indexOf(homeAddress);
			homeAddresses.remove(i);
			careOfAddresses.remove(i);
			expiryTimers.remove(i);
		} else {
			//d.print("FA storing homeAdr: '"+homeAddress+"' careOfAdr: '"+careOfAddress+"' expiryTimer: '"+expiryTimer+"'");
			
			// delete old values
			if (homeAddresses.contains(homeAddress)){
				homeAddresses.remove(homeAddress);
				careOfAddresses.remove(careOfAddress);
				expiryTimers.remove(expiryTimer);
			}

			homeAddresses.add(homeAddress);
			careOfAddresses.add(careOfAddress);
			expiryTimers.add(expiryTimer);
		}

		d.print("FA forwarding MRQMessage to HomeAgent");
		sendMessage(ipMessage, this);

		printMobileHostsTable();
	}


	protected String getNewDestination(String destination){
		return null;
	}


	public void printMobileHostsTable(){
		int i;
		String output = "\n";
		boolean print = false;

		output += "---------------\n";
		output += "homeA  | c-o-a \n";
		output += "---------------\n";

		for (i=0; i<homeAddresses.size(); i++){
			output += (String)homeAddresses.get(i)+" | "+(String)careOfAddresses.get(i)+"\n";
		}
		output += "---------------\n";
		
		if (print)
			d.print(output);	
	}
}
