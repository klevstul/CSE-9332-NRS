// ------------------------------------------------------
// filename:	Router.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		15.10.2003
// version:		20031018
// description: The parent for all routers (basic, HA, FA)
// ------------------------------------------------------

package node;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;
import java.util.Random;
import java.util.Timer;

import utility.Debug;
import utility.Parser;
import utility.Message;
import message.RIPMessage;
import routingtable.RoutingTable;
import task.PeriodicUpdate;


public abstract class Router extends Node {

	private Debug d;
	private Parser parser;
	private Message msg;
	private RoutingTable routingTable;
	private Vector neighbours;
	private Timer timer;
	private String routerID;
	private Vector connectedNodes;


	protected void initialise(String routerID_){
		routerID = routerID_;
		String filename = "config/"+routerID+".cfg";
		String neighbourAddress;
		String neighbourHopCount;
		RIPMessage configRIPMessage = new RIPMessage(routerID);
		int i;

		
		// --------------------------
		// initialise different variables
		// --------------------------
		d				= new Debug("Router '"+routerID+"'");
		parser 			= new Parser();
		msg				= new Message("Router '"+routerID+"'");
		routingTable	= new RoutingTable(routerID);
		neighbours		= new Vector();
		connectedNodes	= new Vector();

		//d.print("Object created");
		

		// --------------------------
		// Create a config RIP message from the values in the routers config file
		// --------------------------
		//d.print("Initilise with values from file '"+filename+"'");
		try {
			String line;
			BufferedReader reader = new BufferedReader(new FileReader(filename));

			line = reader.readLine();
			while (line != null){
				if (line.startsWith("#")) {
					// do nothing
				} else {
					if (parser.noTokens(line, " ")==2) {
						
						// the address of the router (same as listening port)
						if (line.startsWith("l"))
							setAddress(parser.parse(line," ")[1]);
						
					} else if (parser.noTokens(line, " ")==3) {
						if (line.startsWith("n")){
							// create a RIP message with config information
							neighbourAddress = parser.parse(line," ")[1];
							neighbourHopCount = parser.parse(line," ")[2];
							configRIPMessage.addEntry(neighbourAddress, neighbourHopCount);
							
							// add neighbour
							neighbours.add(neighbourAddress);
						}
					}
				}
				line = reader.readLine();
			}			
			reader.close();
		} catch (java.io.IOException e){
			msg.error("Cannot open the file '"+filename);
		} 


		// --------------------------------
		// Add config RIP message to the routing table
		// --------------------------------
		routingTable.addEntry(configRIPMessage, "-");
		routingTable.printTable();


		// --------------------------------------------------------
		// Start listening on the port the router is connected to
		// --------------------------------------------------------
		startListner(getAddress(), this, this);
	}


	protected void run(){
		//d.print("Router in running mode");
		periodicUpdate();
	}


	// -----------------------------------------------------------------
	// the periodic update, to exchange routing tables with neighbours
	// -----------------------------------------------------------------
	public void periodicUpdate(){
		PeriodicUpdate periodicUpdate = new PeriodicUpdate(this);

		// periodic 
	    Random random = new Random();
	    
	    // delay in milliseconds (from 25000 - 35000)
		long timeLimit = Math.abs(random.nextLong()) % 10000 + 25000;
		//long timeLimit 	= Math.abs(random.nextLong()) % 1000 + 2500; // JUST FOR TESTING PURPOSES....
		timer       	= new Timer();

		//d.print("Scheduling a periodic update task ("+Long.toString(timeLimit)+" msec)");
		timer.schedule(periodicUpdate, timeLimit);
	}
	
	
	// ----------------------------------------
	// cancel / reset periodic update timer
	// ----------------------------------------
	public void cancelTimer(){
		timer.cancel();
	}
	
	
	public Vector getNeighbours(){
		return neighbours;
	}


	public RoutingTable getRoutingTable(){
		return routingTable;
	}

	
	// --------------------------------
	// process received messages
	// --------------------------------
	public void processMessage(String source, String destination, byte[] data){
		String identifier		= new String(data, 0, 3);


		if (identifier.equals("RIP")){
			int noentries			= data[3];
			String ripDestination;
			String ripDistance;
			int i;
			int destoffset;
			int distoffset;
			RIPMessage ripMessage	= new RIPMessage(routerID);

			//d.print("Msg identified as a RIP response message from source '"+source+"' with '"+noentries+"' entries");
			
			// go through all entries in the message, add them to a RIPMessage
			for (i=0; i<noentries; i++){
				destoffset = 6+(7*i);
				distoffset =11+(7*i);
				ripDestination = new String(data, destoffset, 5);
				ripDistance	= new String(data, distoffset, 2);
				//d.print("dest("+destoffset+"):"+destination+" dist("+distoffset+"):"+distance);
				ripMessage.addEntry(ripDestination, ripDistance);
			}

			// is routing table is empty, we have to wait processing the message
			// until it has been initialised with values from the config file
			if (!routingTable.isEmpty()){
				// add the RIPMessage to the router's routing table
				//d.print("add entry...");
				routingTable.addEntry(ripMessage, source);
			} else {
				d.print("package dropped, router not initialised yet");
			}

		} else if (identifier.equals("ADR")) {
			sendADMessage(source);
		} else if (identifier.equals("MRQ")) {
			processMRQMessage(source, destination, data);
		} else {
			msg.error("Unidentified message: '"+identifier+"'");
		}

		routingTable.printTable();
	}

	
	public String getRouterID(){
		return routerID;
	}


	public Vector getConnectedNodes(){
		return connectedNodes;
	}

	
	protected String getNewIPAddress(){
		int i = connectedNodes.size();
		String newIPAddress;

		// find avaiable IP address for node
		i++;
		if (i>9){
			msg.error("Router is full, cannot connect node.");
		}
		
		int ipAddress = Integer.parseInt(getAddress());
		ipAddress += i;

		newIPAddress = (new String()).valueOf(ipAddress);

		// add node to list of known nodes
		connectedNodes.add(newIPAddress);
		
		// return new IPAddress
		return newIPAddress;
	}

	public boolean connectedToThisRouter(String ipAddress){
		int i;
		
		for (i=0; i<connectedNodes.size(); i++){
			if (ipAddress.equals((String)connectedNodes.get(i)))
				return true;
		}
		return false;
	}


	// ------------------------------------
	// send an Agent Discovery Message
	// ------------------------------------
	protected abstract void sendADMessage(String destination);

	
	// ------------------------------------
	// process a Mobile ip ReQuest message
	// ------------------------------------
	protected abstract void processMRQMessage(String source, String destination, byte[] data);


	// ------------------------------------
	// in case that ip address belongs to a mobile host on a foreign network 
	// we might have to change destination of the package (if router is a HA)
	// ------------------------------------
	protected abstract String getNewDestination(String destination);

}
