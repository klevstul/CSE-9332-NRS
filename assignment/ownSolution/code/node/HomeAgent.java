// ------------------------------------------------------
// filename:	HomeAgent.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		22.10.2003
// version:		20031022
// description:	Implements a home agent (HA) router
// ------------------------------------------------------

package node;

import java.util.Vector;

import utility.Debug;
import message.ADMessage;
import message.IPMessage;
import message.MREMessage;


public class HomeAgent extends Router {

	private Debug d;
	Vector careOfAddresses;
	Vector homeAddresses;


	public HomeAgent(String routerID){
		d 				= new Debug("HomeAgent '"+routerID+"'");
		careOfAddresses = new Vector();
		homeAddresses 	= new Vector();
		
		d.print("Object created");
		
		// initialise routing table and set neighbours 
		initialise(routerID);
		
		// go to running mode
		run();
	}


	protected void sendADMessage(String destination){
		d.print("Sending an Agent Discovery Message to '"+destination+"'");
		ADMessage adMessage = new ADMessage("HFA");
		IPMessage ipMessage = new IPMessage(getAddress(),destination,adMessage.flushAsBytes());
		sendMessage(ipMessage, this);
	}


	protected void processMRQMessage(String source, String destination, byte[] data){
		String homeAddress = new String(data, 3,5);
		String careOfAddress = new String(data, 8,5);
		String expiryTimer = new String(data, 13,5);
		MREMessage mreMessage = null;			
		int i;

		d.print("MRQmessage received");

		if (expiryTimer.equals("00000")){
			mreMessage = new MREMessage("ACK_dereg");
			//d.print("De-registration, removing all '"+homeAddress+"' entries in Home Agent");
			i = homeAddresses.indexOf(homeAddress);
			homeAddresses.remove(i);
			careOfAddresses.remove(i);
		} else {
			mreMessage = new MREMessage("ACK_reg");
			//d.print("HA storing homeAdr: '"+homeAddress+"' careOfAdr: '"+careOfAddress+"'");

			// delete old values
			if (homeAddresses.contains(homeAddress)){
				homeAddresses.remove(homeAddress);
				careOfAddresses.remove(careOfAddress);
			}

			homeAddresses.add(homeAddress);
			careOfAddresses.add(careOfAddress);
		}
		IPMessage ipMessage = new IPMessage(getAddress(),careOfAddress,mreMessage.flushAsBytes());

		d.print("HA forwarding MREMessage to Mobile Node");
		sendMessage(ipMessage, this);

		printMobileHostsTable();
	}


	protected String getNewDestination(String destination){
		int i;
		
		for (i=0; i<homeAddresses.size(); i++){
			if (destination.equals((String)homeAddresses.get(i)))
				return (String)careOfAddresses.get(i);
		}
		return null;
	}


	public void printMobileHostsTable(){
		int i;
		String output = "\n";
		boolean print = false;

		output += "---------------\n";
		output += "homeA  | c-o-a \n";
		output += "---------------\n";

		for (i=0; i<homeAddresses.size(); i++){
			output += (String)homeAddresses.get(i)+" | "+(String)careOfAddresses.get(i)+"\n";
		}
		output += "---------------\n";
		
		if (print)
			d.print(output);	
	}
}
