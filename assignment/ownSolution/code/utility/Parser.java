// ------------------------------------------------------
// filename:	Parser.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		16.10.2003
// version:		20031015
// description: Parses a string
// ------------------------------------------------------

package utility;


import java.util.StringTokenizer;



public class Parser {


    static public String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }
		return tokens;
    }



    static public int noTokens(String str, String delim){
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
	    while (st.hasMoreTokens()) {
			st.nextToken();
			i++;
	    }
		return i;
    }

}