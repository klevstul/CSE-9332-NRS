// ------------------------------------------------------
// filename:	Message.java
// author:		Frode Klevstul (frode@klevstul.com)
// started:		18.10.2003
// version:		20031018
// description: Prints out different messages
// ------------------------------------------------------

package utility;



public class Message {

	private String referer;

	public Message(String referer_){
		referer = referer_;
	}


	public void error(String string){
		System.out.println("ERROR in '"+referer+"': "+string);
		System.out.println("Terminating program...");
		System.exit(1);
	}

	public void print(String string){
		System.out.println(""+string);
	}

}
